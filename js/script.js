window.addEventListener("load", function () { 
    const loader = document.querySelector(".loader");
    loader.className += " hidden";
 })
$(document).ready(function () { 

    //Image zoom effect


    //carousel
    $(".sidenav").sidenav();
    $('.carousel.carousel-slider').carousel();
    autoplay();
    //Carousel Mobile

    //Parallax
    $('.parallax').parallax();
    if(window.matchMedia("(min-width: 993px)").matches){
        //Modals
        $('.modal').modal();
        //MaterialBox image
        var elem = document.querySelectorAll('.materialboxed');
        var instance = M.Materialbox.init(elem, {
        onOpenStart: function() {
            $('.materialboxed').removeClass('circle');
            $('.enterprise-text').css('display', 'none');
        },
        onCloseStart: function() {
            $('.materialboxed').addClass('circle');
        }
        });
    }

    //Slick
    $('.slick-container').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        adaptativeHeight: true,
        mobileFirst: true,
    });
    //top arrow button
    $('#top-arrow').css('display','none');
    $("#top-arrow").click(function(){
        $('html, body').animate({ scrollTop : 0}, 800);
    });

    $(window).scroll(function(){
        if ($(this).scrollTop() > 80){
            $('#top-arrow').fadeIn();
        }else{
            $('#top-arrow').fadeOut();
        }
    })
});

function autoplay(){
    $('.carousel.carousel-slider').carousel('next');
    setTimeout(autoplay, 4500);
}

